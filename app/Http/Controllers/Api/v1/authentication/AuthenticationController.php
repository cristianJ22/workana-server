<?php

namespace App\Http\Controllers\Api\v1\authentication;

use Throwable;
use Carbon\Carbon;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Api\v1\authentication\AuthRequest;
use App\Http\Requests\Api\v1\authentication\RegisterRequest;

class AuthenticationController extends Controller
{
    use ApiResponser;

    /**
     * login user
     *
     * @param  mixed $request
     * @return void
     */
    public function login(AuthRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if (!User::where('email', $credentials['email'])->exists()) {
            return response()->json([
                'msg' => 'User does not exist'
            ], 401);
        }

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'msg' => 'User or Password are invalid.'
            ], 401);
        }

        $access_token = Auth::user()->createToken(config('SECRET_KEY'));
        $expiration = $access_token->token->expires_at->diffInSeconds(Carbon::now());

        return $this->successResponse('ok', 202, 'login success', [
            'user' => Auth::user(),
            'access_token' => $access_token,
            'token_expiration_in_seconds' => $expiration
        ]);
    }

    /**
     * register user
     *
     * @param  mixed $request
     * @return Response
     */
    public function register(RegisterRequest $request)
    {
        DB::beginTransaction();
        try {
            $password = Hash::make($request->password);
            $user_data = array_merge($request->except(['password', 'confirmation_password']), ['password' => $password]);

            User::create($user_data);
            DB::commit();

            return response()->json([
                'msg' => [
                    'user' => ['User created successfully']
                ]
            ], 201);
        } catch (Throwable $th) {
            DB::rollBack();
            return response()->json([
                'msg' => [
                    'user' => ['The user was not created!']
                ]
            ], 500);
        }
    }

    /**
     * revokeToken
     *
     * @param Request $request
     * @return Response
     */
    public function revokeToken(Request $request)
    {
        $result = $request->user()->token()->revoke();
        return $this->successResponse('ok', 202, 'User logout success', [
            'logout' => $result
        ]);
    }
}
